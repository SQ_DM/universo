extends Control


# Обработчик сигнала "message_received"
func output_message(message):
	$OutputArea.insert_text_at_cursor(message + "\n")

# Выполняется при открытии сцены
func _on_Options_tree_entered():
	# Регистрируем обработчик сигнала "message_received", если ещё 
	# не зарегистрирован
	if !Net.is_connected("message_received", self, "output_message"):
		Net.connect("message_received", self, "output_message")

# Выполняется перед выходом из сцены
func _on_Options_tree_exiting():
	# Убираем обработчик сигнала "message_received", если зарегистрирован
	if Net.is_connected("message_received", self, "output_message"):
		Net.disconnect("message_received", self, "output_message")
