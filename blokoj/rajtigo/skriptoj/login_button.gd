extends Button


const QueryObject = preload("queries.gd")


func _pressed():
	var password = $'../your_password'.text
	var login = $'../your_login'.text

	Global.login = login

	var Q = QueryObject.new()

	$HTTPRequest.request(Q.URL, Q.headers, false, 2, Q.auth_query(login, password))
