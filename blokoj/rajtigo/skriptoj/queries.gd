extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "rajtigo"

# URL к API
const URL = "https://tehnokom.su/api/v1.1/registrado/"
# Необходимые заголовки
const headers = ["Content-Type: application/json"]
# Запрос к API
func auth_query(login, password):
	return JSON.print({ "query": "mutation { ensaluti(login: \"%s\", password: \"%s\") { status, token, message, csrfToken } }" % [login, password] })
