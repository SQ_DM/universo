extends ItemList


# Обработчик выбора элемент списка ItemList
func _on_ItemList_item_selected(index):
	var SelectedText = $"../../WindowDialog".ItemListContent[index]
	# Назначаем текст виджету DetailLabel
	$"../../WindowDialog".get_node("DetailLabel").set_text(SelectedText)

