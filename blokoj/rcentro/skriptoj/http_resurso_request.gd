extends HTTPRequest


# Обработчик ответа на HTTP запрос к бэкэнду
func _on_HTTPRequestFind_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	var simpled_data = parsed_resp['data']['resursoj']['edges']
	
	$"../../WindowDialog".ItemListContent.clear()
	$"../../WindowDialog".get_node("ItemList").clear()
	$"../../WindowDialog".get_node("DetailLabel").set_text("")
	
	for item in simpled_data:
		$"../../WindowDialog".ItemListContent.append(item['node']['nomo']['enhavo'])
	
	$"../../WindowDialog".FillItemList()
