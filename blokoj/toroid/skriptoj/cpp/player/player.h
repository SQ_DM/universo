#include <Godot.hpp>
#include <KinematicBody.hpp>
#include <Input.hpp>

namespace godot
{
    class Player: public KinematicBody {
        GODOT_CLASS(Player, KinematicBody);

        private:
        
        const int SPEED = 300;
        
        public:

        static void _register_methods();
        void _init();
        void _physics_process(float delta);

        Player();
        ~Player();
    };
} // namespace godot

